﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace TestFramework
{
   public class TestEngine
   {
      public TestRunResult RunTests( object[] testObjects, IProgressWriter writer )
      {
         var testCollection = new TestCollection();
         var extractor = new TestExtractor();

         foreach ( var testObject in testObjects )
         {
            testCollection.AddRange( extractor.ExtractTestMethods( testObject ) );
         }

         return this.Run( testCollection, writer );
      }

      public TestRunResult RunTests( object testObject, IProgressWriter writer )
      {
         var extractor = new TestExtractor();
         var testCollection = extractor.ExtractTestMethods( testObject );

         return this.Run( testCollection, writer );
      }

      private TestRunResult Run( TestCollection testCollection, IProgressWriter writer )
      {
         writer.WriteLine( "Found {0} test{1} to run.", testCollection.Count, testCollection.Count == 1 ? string.Empty : "s" );

         var testResults = new List<TestResult>();

         foreach ( var test in testCollection )
         {
            var methodFullName = String.Format( "{0}.{1}", test.MethodInfo.DeclaringType.Name, test.MethodInfo.Name );
            var testMethod = (Action) Delegate.CreateDelegate( typeof( Action ), test.Instance, test.MethodInfo.Name );

            try
            {
               testMethod();
            }
            catch ( Exception ex )
            {
               testResults.Add( TestResult.Failed( methodFullName, ex ) );

               var originationSite = string.Empty;

               if ( ex.GetType() != typeof( TestException ) )
               {
                  var regex = new Regex( @" in ([A-Za-z]:[^:]*):line ([\d]*)" );
                  var match = regex.Match( ex.StackTrace );

                  if ( match.Success && match.Groups.Count >= 3 )
                  {
                     originationSite = String.Format( " at {0}:line {1}", match.Groups[1].Value, match.Groups[2].Value );
                  }
               }

               writer.WriteLine( "{0}/{1} failed {2}{3} \"{4}\"", testResults.Count, testCollection.Count, methodFullName, originationSite, ex.Message );
               System.Diagnostics.Trace.WriteLine( ex );
               continue;
            }

            testResults.Add( TestResult.Suceeded( methodFullName ) );
            writer.WriteLine( "{0}/{1} successful {2}.", testResults.Count, testCollection.Count, methodFullName );
         }

         var result = new TestRunResult( testResults.ToArray() );

         if ( result.FailedTests > 0 )
         {
            writer.WriteLine( "*** {0}/{1} test{2} failed! ***", result.FailedTests, result.TotalTests, result.TotalTests == 1 ? string.Empty : "s" );
         }
         else
         {
            writer.WriteLine( "{0}/{1} test{2} succeeded.", result.SuccessfulTests, result.TotalTests, result.TotalTests == 1 ? string.Empty : "s" );
         }

         return result;
      }
   }

   #region Test Attributes

   public class TestMethod : System.Attribute
   {
   }

   #endregion
}